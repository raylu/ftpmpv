﻿using System;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace ftpmpv {
	public partial class MainWindow : Window {
		string currentPath;

		public MainWindow() {
			InitializeComponent();
		}

		private void Window_Loaded(object sender, RoutedEventArgs e) {
			TreeViewItem tvi = new TreeViewItem() {Header = "/"};
			dirs.Items.Add(tvi);
			listDir("/", tvi);
		}

		void listDir(string path, TreeViewItem tree) {
			FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://xis" + path);
			request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
			FtpWebResponse response = (FtpWebResponse)request.GetResponse();
			StreamReader sr = new StreamReader(response.GetResponseStream());

			files.Items.Clear();
			tree.Items.Clear();

			while (true) {
				string line = sr.ReadLine();
				if (line == null || line.Length == 0)
					break;
				string[] split = line.Split(new char[]{' '}, 9, StringSplitOptions.RemoveEmptyEntries);

				string mode = split[0];
				string name = split[8];
				if (mode[0] == 'd') {
					TreeViewItem tvi = new TreeViewItem() {Header = name};
					tree.Items.Add(tvi);
				} else
					files.Items.Add(name);
			}

			tree.ExpandSubtree();
			currentPath = path;
		}

		private void dirs_KeyUp(object sender, KeyEventArgs e) {
			if (e.Key == Key.Enter)
				dirsActivate();
		}

		private void dirs_MouseDoubleClick(object sender, MouseButtonEventArgs e) {
			dirsActivate();
		}

		private void dirsActivate() {
			TreeViewItem tvi = (TreeViewItem)dirs.SelectedItem;

			string path = "";
			TreeViewItem cur = tvi;
			while (true) {
				path = "/" + (string)cur.Header + path;
				if (cur.Parent != null && cur.Parent.GetType() == typeof(TreeViewItem))
					cur = (TreeViewItem)cur.Parent;
				else
					break;
			}
			path += "/";

			listDir(path, tvi);
		}

		private void files_KeyUp(object sender, KeyEventArgs e) {
			if (e.Key == Key.Enter)
				filesActivate();
		}

		private void files_MouseDoubleClick(object sender, MouseButtonEventArgs e) {
			filesActivate();
		}

		private void filesActivate() {
			string filename = (string)files.SelectedItem;
			Console.WriteLine("ftp://xis" + currentPath + filename);
			System.Diagnostics.Process.Start(@"D:\PRGMs\mpv\mpv.exe", "--cache=no --demuxer-seekable-cache=yes --demuxer-readahead-secs=1 --demuxer-thread=yes \"ftp://xis" + currentPath + filename + "\"");
		}
	}
}
